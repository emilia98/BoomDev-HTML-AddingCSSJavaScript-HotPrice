import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  let hotPrices = document.querySelectorAll(".hot");
  hotPrices.forEach(hotPrice => {
    hotPrice.textContent += "🔥";
  });
});